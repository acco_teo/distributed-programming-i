#include    <stdlib.h>
#include    <string.h>
#include    <sys/stat.h>
#include    <time.h>
#include    <libgen.h>
#include    "../errlib.h"
#include    "../sockwrap.h"

#define COMM_ERR "-ERR\r\n"
#define OK_MSG "+OK\r\n"
#define GET_MSG "GET "
#define MAX_BUFL 2048
#define BACKLOG 128
#define MAX_FN 256
#define TIMEOUT 15

char *prog_name;

int files_transfer();
int stat_infoes(char *fn, int* size, time_t* modtime);

int main (int argc, char** argv)
{
    struct sockaddr_in srv_addr, cli_addr;
    int listenfd, connfd, eos, pid;
    socklen_t cli_addrlen;
    short port;
    
    /* requested by errlib */
    prog_name = argv[0];

    /* checking arguments correctness */
    if (argc != 2)
        err_quit("usage: %s <port_number>", prog_name);

    port = atoi(argv[1]);
    
    /* checking port range correctness */
    if(port > 65535)
    	err_quit("usage: 1024 <= <port_number> <= 65535", prog_name);

    /* create socket opaque */
    listenfd = Socket(AF_INET, SOCK_STREAM, 0);

    /* specify address to bind to */
    memset(&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family      = AF_INET;
    srv_addr.sin_port        = htons(port);
    srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    Bind(listenfd, (SA*) &srv_addr, sizeof(srv_addr));

    Listen(listenfd, BACKLOG);
    
    /* Instantiating signal handler in order to avoid zombie process */
    signal(SIGCHLD, SIG_IGN);

    fprintf(stdout, "(%s) (ok) socket created\n", prog_name);
    fprintf(stdout, "(%s) (ok) listening on %s:%u\n", prog_name, inet_ntoa(srv_addr.sin_addr), ntohs(srv_addr.sin_port));

    for(;;) { /* infinite server loop */

        fprintf(stdout, "(%s) (sys) waiting for clients...\n", prog_name);
        cli_addrlen = sizeof(struct sockaddr_in);
        connfd = myAccept (listenfd, (SA*) &cli_addr, &cli_addrlen);
        fprintf(stdout, "(%s) (ok) accepted connection from %s:%u\n", prog_name, inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
        
        /* Managing concurrency (process creation on demand) */
	if ((pid = fork()) < 0)
	    fprintf(stderr, "(%s) (err) fork() failed", prog_name);
	else if(pid > 0) {
	    /* Parent (master) -> close active socket and accept new requests */
	    close(connfd);
	} else {
	    /* Child (slave)-> close passive socket, serve client and close socket */
	    close(listenfd);
	    eos = files_transfer(connfd);
	    if(eos < 0) {
	        /* some error in protocol occourred */
	        if(writen(connfd, COMM_ERR, strlen(COMM_ERR)) < 0)
            	    fprintf(stdout, "(%s) (err) impossible to send COMM_ERR\n", prog_name);
	    }
	    fprintf(stdout, "(%s) (sys) connection closed\n", prog_name);
	    close(connfd);
	    exit(0);
	}
    }
    return 0;
}

int files_transfer(int connfd)
{
    int checked, msg_size, advance = 0, n, filesize, cont_flag, read_bytes, tot_bytes;
    char buf[MAX_BUFL-1], sbuf[MAX_BUFL], filename[MAX_FN];
    struct timeval tv;
    time_t modtime;
    uint32_t fs, t;
    fd_set rset;
    char *tmp;
    FILE *fp;
    
    while (1) {     /* infinite service loop, exit when transfer is ended */
    
    	fprintf(stdout, "(%s) (sys) waiting for requests...\n", prog_name);
    	
    	FD_ZERO(&rset);
    	FD_SET(connfd, &rset);
    	tv.tv_sec = TIMEOUT;
    	tv.tv_usec = 0;
    	
    	/* clearing buffer paying attention to not delete a possible surplus of a previous recv */
    	memset(buf+advance, '\0', MAX_BUFL-advance);
    	checked = 0;
    	
    	if (select(FD_SETSIZE, &rset, NULL, NULL, &tv) > 0) {
    	
    	    /* retrieving new message/s and checking errors*/
    	    n = recv(connfd, buf+advance, MAX_BUFL-advance-1, 0);    
    	    
    	    if(n < 0) {
    	    	fprintf(stdout, "(%s) (err) recv error\n", prog_name);
    	    	return 1;
    	    }
    	    else if(n == 0) {
    	    	fprintf(stdout, "(%s) (ok) client closed connection gracefully\n", prog_name);
    	    	return 0;
    	    }
    	    
    	    /* adding null terminator (string utils) */
    	    buf[n+advance] = '\0';
    	    
    	    /* managing more than one request in a single buffer read */
    	    while((tmp = strstr(buf + checked, "\n")) != NULL) {
    	    
    	    	msg_size = n + advance - strlen(tmp) - checked + 1;  /* (+1 including \n) */

    	    	/* checking request validity (GET filename.xyz\r\n) */
    	    	if(strncmp(buf+checked, GET_MSG, strlen(GET_MSG)) != 0 && buf[msg_size+checked-1] != '\n' && buf[msg_size+checked-2] != '\r') {
    	    	    fprintf(stdout, "(%s) (err) message format invalid, \n", prog_name);
    	    	    return -1;
    	    	}
    	    	
    	    	/* extracting filename from request message (-2 cleaning it from CR LF) */
    	    	memset(filename, '\0', sizeof(filename));
    	    	memcpy(filename, buf+checked+strlen(GET_MSG), msg_size-strlen(GET_MSG)-2);
    	    	
    	    	/* incrementing checked pointer */
    	    	checked += msg_size;
    	    	
    	    	fprintf(stdout, "(%s) (ok) client asked to send file '%s'\n", prog_name, filename);
    	    	
    	    	/* checking filename validity */
    	    	if((fp = fopen(filename, "rb")) == 0 || strstr(filename, "/") != NULL) {
    	    	    fprintf(stdout, "(%s) (err) %s doesn't exist, shutting down connection...\n", prog_name, filename);
                    return -2;
                }
                
                /* retrieving file informations (size and last modification time) */
                if(!stat_infoes(filename, &filesize, &modtime)) {
                    fprintf(stdout, "(%s) (err) impossible to retrieve file statistics, shutting down connection...\n", prog_name);
                    return -3;
                }
                
                /* transforming size and timestamp in 32 bit unsigned int, network format */
                fs = htonl(filesize);
                t = htonl(modtime);
                
                /* sending +OK\r\n */
                Send(connfd, OK_MSG, strlen(OK_MSG), 0);
                /* sending fileseize 4 byte network order */
                Send(connfd, &fs, sizeof(fs), 0);
                /* reading and sending file */
                cont_flag = 0;
                read_bytes = 0;
                
                for(tot_bytes=0; tot_bytes < filesize;) {
                    read_bytes = readn(fileno(fp), sbuf, (filesize-tot_bytes < MAX_BUFL) ? filesize-tot_bytes : MAX_BUFL); 	/* reading always the correct amount */
                    tot_bytes += read_bytes;
                    if(sendn(connfd, sbuf, read_bytes, 0) == -1) {	/* handling a possible SIGPIPE */
                       	cont_flag = -1;
                        break;
                    }
                }
                
                fclose(fp); 	/* closing file descriptor ASAP */
                
                if(cont_flag == -1) {
                    fprintf(stdout, "(%s) (err) failure dued to a SIGPIPE, shutting down connection...\n", prog_name);
                    return 0;	/* return 0 because the pipe is broken and a try to send COMM_ERR would fail */
                }
                
                /* sending timestamp 4 byte network order */
                Send(connfd, &t, sizeof(t), 0);
                
                fprintf(stdout, "(%s) (ok) %s correctly tansferred\n", prog_name, filename);
            }
            
            /* handling possible broken message due fixed size of buf (see recv) */
            if(checked != n+advance) {
            	/* possibly there is a broken message: 
            	 * 	-coping the surplus into the starting position of buf
            	 *	-saving the surplus length
            	 *	-back to recv, waiting for the message residue
            	 */
            	memcpy(buf, buf+checked, n+advance-checked+1);
            	advance = strlen(buf);
            }
            else {
            	/* all the requests in the buffer have been served */
            	advance = 0;
            }
        }
        else {
            fprintf(stdout, "(%s) (sys) timer expired, shutting down connection...\n", prog_name);
            return -4;
        }  
    }
}

int stat_infoes(char *fn, int* size, time_t* modtime) {
    struct stat fileinfo;
    if (stat(fn, &fileinfo) == 0){
        *modtime = fileinfo.st_mtime;
        *size    = fileinfo.st_size;
        return 1;
    }
    return 0;
}

