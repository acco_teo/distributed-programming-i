#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../sockwrap.h"
#include "../errlib.h"

#define OK_MSG "+OK\r\n"
#define COMM_ERR "-ERR"
#define MAX_BUFL 2048
#define MAX_REQL 260
#define TIMEOUT 15

char *prog_name;

struct transfer_result {
	int valid;
	uint32_t filesize;
	time_t timestamp;
};

struct transfer_result file_receiver(int sockfd, const char* filename);

int main(int argc, char **argv)
{
    struct transfer_result t;
    char buf[MAX_REQL];
    struct timeval tv;
    int sockfd, i;
    fd_set rset;

    /* requested by errlib */
    prog_name = argv[0];

    /* checking parameters correctness */
    if (argc < 4)
        err_quit ("usage: %s <server> <port_number> <file1> <file2> ... <filen>", prog_name);

    for(i=3; i<argc; i++) {
        if(strlen(argv[i]) >= MAX_REQL-4)
            err_quit("error: %s is an invalid filename (MAX: 255 chars)", argv[i]);
    }
    
    /* checking port range correctness */
    if(atoi(argv[2]) < 1024 || atoi(argv[2]) > 65535)
    	err_quit("usage: 1024 <= <port_number> <= 65535", prog_name);

    /* creating socket and connecting to server */
    sockfd = Tcp_connect(argv[1], argv[2]);
    
    /* setting timeout structure for sockopt timers */
    tv.tv_sec = TIMEOUT;
    tv.tv_usec = 0;
    setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
    setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(tv));

    fprintf(stdout, "(%s) (ok) connected to %s:%s\n", prog_name, argv[1], argv[2]);

    for(i=3; i<argc; i++) {

        /* clearing command buffer */
        memset(buf, '\0', sizeof(buf));

        /* building requests messages, format is 'GET filename/r/n' */
        snprintf(buf, sizeof(buf), "GET %s\r\n", argv[i]);

        /* sending request message */
        Writen(sockfd, buf, strlen(buf));

        /* instantiating select structure (double check with sockopt) */
        FD_ZERO(&rset);
        FD_SET(sockfd, &rset);
        
        /* waiting TIMEOUT sec for messages */
        if (select(FD_SETSIZE, &rset, NULL, NULL, &tv) > 0) {
        
            t = file_receiver(sockfd, argv[i]);
            
            if(t.valid == 1) {
            	/* printing resume, transfer is ok */
            	fprintf(stdout, "(%s) (ok) filename: \t%s\n", prog_name, argv[i]);
            	fprintf(stdout, "(%s) (ok) size: \t\t%u\n", prog_name, t.filesize);
            	fprintf(stdout, "(%s) (ok) timestamp: \t%ld\n", prog_name, t.timestamp);
            }
            else {
            	fprintf(stdout, "(%s) (err) received file is incomplete, deleting file...\n", prog_name);
            	/* safety reason, the manual suggests to check the return value */
            	if(remove(argv[i]) == 0)
            	    fprintf(stdout, "(%s) (ok) file deleted correctly\n", prog_name);
            	else
            	    fprintf(stdout, "(%s) (err) an error occurred during deletion\n", prog_name);
            	break;
            }
        }	
        else {
            fprintf(stdout, "(%s) (sys) timer expired, closing connection...\n", prog_name);
            break;
        }
    }
    
    /* closing socket */
    Close(sockfd);
    
    exit(0);
}

struct transfer_result file_receiver(int sockfd, const char* filename) 
{
    struct transfer_result tr;
    int read_bytes, tot_bytes;
    char rbuf[MAX_BUFL];
    FILE *fp;
    
    tr.valid = 0;
    
    memset(rbuf, '\0', sizeof(rbuf));
    
    /* reading message type and correctness checking */
    Readn(sockfd, rbuf, strlen(OK_MSG));
   
    if(strstr(rbuf, COMM_ERR) != NULL){
    	fprintf(stdout, "(%s) (err) communication error, closing connection...\n", prog_name);
    	return tr;
    }
    else if(strncmp(OK_MSG, rbuf, strlen(OK_MSG)) != 0) {
    	fprintf(stdout, "(%s) (err) message format invalid, closing connection...\n", prog_name);
    	return tr;
    }

    /* reading file size and converting it into correct format */
    Readn(sockfd, rbuf, 4);
    tr.filesize = ntohl((*(uint32_t *)rbuf));
    
    fp = fopen(filename, "wb");

    /* reading the file from the socket descriptor and writing it on file */
    for(tot_bytes=0; tot_bytes < tr.filesize;) {
    	read_bytes = Readn(sockfd, rbuf, (tr.filesize-tot_bytes < MAX_BUFL) ? tr.filesize-tot_bytes : MAX_BUFL);
    	if(read_bytes == ((tr.filesize-tot_bytes < MAX_BUFL) ? tr.filesize-tot_bytes : MAX_BUFL)) {
    	    tot_bytes += read_bytes;
    	    fwrite(rbuf, 1, read_bytes, fp);
    	}
    	else 
    	    break;
    }
    
    fclose(fp);
    
    /* read last modification timestamp */
    Read(sockfd, rbuf, 4);
    tr.timestamp = ntohl((*(uint32_t *)rbuf));
    
    /* checking file completeness */
    if(tot_bytes == tr.filesize)
    	tr.valid = 1;
    	
    return tr;
}
