# File Transfer Protocol

### Sequential (Server1/Client1)
Develop a TCP sequential server (listening to the port specified as the first parameter of the command line, as a decimal integer) that, after having established a TCP connection with a client, accepts file transfer requests from the client and sends the requested files back to the client, following the protocol specified below. The files available for being sent by the server are the ones accessible in the server file system from the working directory of the server. Develop a client that can connect to a TCP server (to the address and port number specified as first and second command-line parameters, respectively). After having established the connection, the client requests the transfer of the files whose names are specified on the command line as third and subsequent parameters, and stores them locally in its working directory. After having transferred and saved locally a file, the client must print a message to the standard output about the performed file transfer, including the file name, followed by the file size (in bytes, as a decimal number) and timestamp of last modification (as a decimal number).

Any timeouts used by client and server to avoid infinite waiting should be set to 15 seconds.
 
The protocol for file transfer works as follows: to request a file the client sends to the server the three ASCII characters “GET” followed by the ASCII space character and the ASCII characters of the file name, terminated by the ASCII carriage return (CR) and line feed (LF):

```
- - - - - - - - - - - - - - - - - - - - 
| G | E | T |  |  filename  | CR | LF |
- - - - - - - - - - - - - - - - - - - -
```
(Note: the command includes a total of 6 ASCII characters, i.e. 6 bytes, plus the characters of the file name). The server responds by sending:

```
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
| + | O | K | B1 | B2 | B3 | B4 |      file      | T1 | T2 | T3 | T4 |
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
```
Note that this message is composed of 5 characters followed by the number of bytes of the requested file (a 32-bit unsigned integer in network byte order - bytes B1 B2 B3 B4 in the figure), followed by the bytes of the requested file contents, and then by the timestamp of the last file modification (Unix time, i.e. number of seconds since the start of epoch, represented as a 32-bit unsigned integer in network byte order - bytes T1 T2 T3 T4 in the figure).

To obtain the timestamp of the last file modification of the file, refer to the syscalls stat or fstat.

The client can request more files using the same TCP connection, by sending several GET commands, one after the other. When it has finished sending commands on the connection, it starts the procedure for closing the connection. Under normal conditions, the connection should be closed gracefully, i.e. the last requested file should be transferred completely before the closing procedure terminates.

In case of error (e.g. illegal command, non-existing file) the server always replies with:

```
- - - - - - - - - - - - - - 
| - | E | R | R | CR | LF |
- - - - - - - - - - - - - - 
```
(6 characters) and then it closes the connection with the client.


### Concurrent (server2)
Develop a concurrent TCP server (listening to the port specified as the first parameter of the command line, as a decimal integer) that, after having established a TCP connection with a client, accepts file transfer requests from the client and sends the requested files back to the client, following the same protocol used in the iterative version. The server must create processes on demand (a new process for each new TCP connection).

### Testing
Try the transfer of a large binary file (100MB) and check that the received copy of the file is identical to the original one (using diff) and that the implementation you developed is efficient in transferring the file in terms of transfer time.

* While a connection is active try to activate a second client against the same server.
* Try to activate on the same node a second instance of the server on the same port.
* Try to connect the client to a non-reachable address.
* Try to connect the client to an existing address but on a port the server is not listening to. Try to stop the server (by pressing ^C in its window) while a client is connected.

