<?php
  /* is the seat already inside the db?
   * a) no  -> insert + return seat_yellow
   * b) yes ->
   *          1) seat booked -> no action + return seat_red
   *          2) seat reserved by others -> update + return seat_yellow
   *          3) seat reserved by me -> delete + return seat_green
  */

  include('functions.php');

  session_start();

  /* check session validity */
  if(checkSessionValidity())
    $username = $_SESSION[$SESSION_PREFIX . 'username'];
  else {
    $status = "error";
    $message = "Invalid session";
    goto end;
  }

  /* check correctness of ajax request */
  if(!isset($_POST['sid'])) {
    $status = "error";
    $message = "Invalid request";
    goto end;
  }

  /* check seat id validity */
  $sid = $_POST['sid'];
  if(!checkSeatValidity($sid)) {
    $status = "error";
    $message = "Invalid seat";
    goto end;
  }

  /* check username validity */
  $connection = connect();
  if(!checkUserValidity($connection, $username)) {
    $status = "error";
    $message = "Invalid username";
    goto end;
  }
  close($connection);

  $connection = connect();
  try {
    mysqli_autocommit($connection, false);
    mysqli_begin_transaction($connection);
    $sid = mysqli_escape_string($connection, $sid);
    $result = mysqli_query($connection, "SELECT * FROM `seat` WHERE `id` = '$sid' FOR UPDATE");
    if(!$result) {
      $status = "error";
      $message = "The comunication with db failed";
      throw new Exception();
    }
    $rows = mysqli_num_rows($result);

    if($rows == 1) {
      /* the seat is in the db */
      $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
      mysqli_free_result($result);
      if(strcmp($row['status'], "B") == 0) {
        /* the seat is already booked -> return seat_red */
        $status = "success";
        $message = "seat_red";
        throw new Exception();
      }
      else {
        $username = mysqli_escape_string($connection, $username);
        if(strcmp($row['username'], $username) == 0) {
          /* the seat is booked by user -> return seat_green */
          $statement = mysqli_stmt_init($connection);
          mysqli_stmt_prepare($statement, "DELETE FROM `seat` WHERE `id` = ?");
          mysqli_stmt_bind_param($statement, 's', $sid);
          if(! mysqli_stmt_execute($statement)) {
            $status = "error";
            $message = "The comunication with db failed";
            throw new Exception();
          }
          mysqli_stmt_close($statement);
          mysqli_commit($connection);
          close($connection);
          $status = "success";
          $message = "seat_green";
        }
        else {
          /* the seat is booked by another user -> return seat_yellow */
          $statement = mysqli_stmt_init($connection);
          mysqli_stmt_prepare($statement, "UPDATE `seat` SET `username` = ? WHERE `id` = ?");
          mysqli_stmt_bind_param($statement, 'ss', $username, $sid);
          if(! mysqli_stmt_execute($statement)) {
            $status = "error";
            $message = "The comunication with db failed";
            throw new Exception();
          }
          mysqli_stmt_close($statement);
          mysqli_commit($connection);
          close($connection);
          $status = "success";
          $message = "seat_yellow";
        }
      }
    }
    else {
      /* the seat is free -> make it reserved */
      $statement = mysqli_stmt_init($connection);
      mysqli_stmt_prepare($statement, "INSERT INTO `seat`(id, username, status) VALUES (?, ?, 'R')");
      mysqli_stmt_bind_param($statement, 'ss', $sid, $username);
      if(! mysqli_stmt_execute($statement)) {
        $status = "error";
        $message = "The comunication with db failed";
        throw new Exception();
      }
      mysqli_stmt_close($statement);
      mysqli_commit($connection);
      close($connection);
      $status = "success";
      $message = "seat_yellow";
    }
  }
  catch(Exception $e) {
    mysqli_rollback($connection);
  }


end:
  $response = array(
    'status' => $status,
    'message' => $message
  );
  header('Content-type: application/json');
  echo json_encode($response);

?>
