<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	/* the user is already logged -> redirect to homepage */
	if(checkSessionValidity()) {
		header("location: index.php");
	}

	if(isset($_REQUEST['submit'])) {
		$connection = connect();
		$statement = mysqli_stmt_init($connection);
		mysqli_stmt_prepare($statement, "SELECT `password` FROM `user` WHERE `username` = ?");
		$user = mysqli_escape_string($connection, $_REQUEST['email']);
		mysqli_stmt_bind_param($statement, 's', $user);
		if(! mysqli_stmt_execute($statement)) {
			$error = "The comunication with db failed.";
			goto displayPage;
		}

		mysqli_stmt_bind_result ($statement, $q_password);

		mysqli_stmt_store_result($statement);

		if (mysqli_stmt_num_rows($statement) == 1) { //To check if the row exists

			if (mysqli_stmt_fetch ($statement)) { //fetching the contents of the row
				if (password_verify($_REQUEST['password'], $q_password)) {
					/* logged in correctly */
					$_SESSION[$SESSION_PREFIX . 'username'] = $user;
					$_SESSION[$SESSION_PREFIX . 'authorized'] = true;
					$_SESSION[$SESSION_PREFIX . 'time'] = time();
					$response['status'] = "success";
					$response['message'] = "Correctly logged in!";
					$_SESSION[$SESSION_PREFIX . 'response'] = $response;
					/* redirect */
					header("location: index.php");
					exit();
				}
				else {
					$error = "Invalid password.";
				}
			}
		}
		else {
			$error = "Invalid username.";
		}

		mysqli_stmt_close($statement);
		close($connection);
	}

	/* jump label */
	displayPage:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
	<title>AirPoli</title>
	<meta name="AirPoli - LogIn" content="Website for AirPoli">
	<meta name="acco_teo" content="DP1_exam">
	<script type="text/javascript" src="functions.js"></script>
</head>
<body>
	<?php
		include('interface.php');
	?>
	<div class="main" id="main">
		<?php
			if(isset($error)) {
				echo "<fieldset class=\"error\"><h4>".sanitizeString($error)."</h4></fieldset><br>";
				unset($error);
			}
		?>
	  <div id="formLogIn" class="formLogIn">
			<form action="login.php" method="post">
				<fieldset>
					<h2>Login!</h2>
					<p>Complete the form in order to create a new account.</p>
					<hr>
					<label>EMAIL</label>
					<input type="email" id="email" name="email" placeholder="Please insert your email.." required="required">
					<label>PASSWORD</label>
					<input type="password" id="password" name="password" placeholder="Please insert your password.." required="required">
					<table style="width: 100%">
						<tr>
							<td><button type="submit" id="submit" name="submit">Submit</button></td>
							<td><button type="reset">Cancel</button></td>
						</tr>
					</table>
				</fieldset>
			</form>
		</div>
	</div>
</body>
</html>
