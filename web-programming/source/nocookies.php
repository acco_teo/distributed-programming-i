<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="AirPoli - nocookies" content="Website for AirPoli">
	<meta name="author" content="acco_teo">
	<title>Error</title>
	<link rel="stylesheet" href="styles.css">
</head>
<body>
	<div class="nocookies">
		<fieldset class="error" style="height:95%; color:black;">
			<h1>Cookies must be enabled to browse this website.<br></h1>
			<h1>Check your browser configuration.</h1>
			<a href="./index.php"><h3>Click here to resume navigation!</h3></a>
		</fieldset>
	</div>
</body>
</html>
