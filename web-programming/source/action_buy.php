<?php
  include('functions.php');
  session_start();

  if(checkSessionValidity())
    $username = $_SESSION[$SESSION_PREFIX . 'username'];
  else {
    $status = "error";
    $message = "Invalid session";
    goto end;
  }
  if(isset($_POST['sids']))
    $sids = json_decode($_POST['sids']);
  else {
    $status = "error";
    $message = "Invalid request";
    goto end;
  }

  /* check username validity */
  $connection = connect();
  if(!checkUserValidity($connection, $username)) {
    $status = "error";
    $message = "Invalid username";
    goto end;
  }
  close($connection);


  $connection = connect();
  try {
    mysqli_autocommit($connection, false);
    mysqli_begin_transaction($connection);

    $username = mysqli_escape_string($connection, $username);
    $query = "SELECT `id` FROM `seat` WHERE `status` = 'R' AND `username` = '$username' FOR UPDATE";
    $result = mysqli_query($connection, $query);

    if($result) {
      $seat_array = array();
      /* buildind db seats_array */
      while ($row = $result->fetch_assoc())
        array_push($seat_array, $row['id']);
      mysqli_free_result($result);

      /* validate received seats */
      foreach($sids as $seat_id) {
        if(checkSeatValidity($seat_id)) {
          /* seat id is valid */
          if(!in_array($seat_id, $seat_array)) {
            /* the seat has been stolen from another user  */
            $statement = mysqli_stmt_init($connection);
            mysqli_stmt_prepare($statement, "DELETE FROM `seat` WHERE `username` = ? AND `status` = 'R'");
            mysqli_stmt_bind_param($statement, 's', $username);
            if(! mysqli_stmt_execute($statement)) {
              $status = "error";
              $message = "The comunication with db failed";
              throw new Exception();
            }
            mysqli_stmt_close($statement);
            mysqli_commit($connection);
            close($connection);
            $status = "error";
            $message = "One or more of your reserved seats has been reserved or booked. All your reserved seats are now free.";
            goto end;
          }
        }
        else {
          /* seat id is not valid */
          $status = "error";
          $message = "One or more of your reserved seats is invalid.";
          goto end;
        }
      }

      /* all seats received from request have been validated */
      $statement = mysqli_stmt_init($connection);
      mysqli_stmt_prepare($statement, "UPDATE `seat` SET `status` = 'B' WHERE `username` = ? AND `status` = 'R'");
      mysqli_stmt_bind_param($statement, 's', $username);
      if(! mysqli_stmt_execute($statement)) {
        $status = "error";
        $message = "The comunication with db failed";
        throw new Exception();
      }
      mysqli_stmt_close($statement);
      mysqli_commit($connection);
      close($connection);
      $status = "success";
      $message = "Seat/s correctly booked!";
    }
    else {
      $status = "error";
      $message = "The comunication with db failed";
      throw new Exception();
    }
  }
  catch(Exception $e) {
      mysqli_rollback($connection);
      goto end;
  }

end:
  $response = array(
    'status' => $status,
    'message' => $message
  );
  $_SESSION[$SESSION_PREFIX.'response'] = $response;
  header('Content-type: application/json');
  echo json_encode($response);
?>
