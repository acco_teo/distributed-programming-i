<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	/* the user is logged -> redirect to homepage */
	if(checkSessionValidity()) {
		header("location: index.php");
	}

	/* server side parameters check validity */
	if(isset($_REQUEST['submit'])) {
		if(! isset($_REQUEST['email'])
		|| ! isset($_REQUEST['password'])
		|| ! isset($_REQUEST['password_confirm'])) {
			$error = "<fieldset class=\"error\"><h4>Please, complete all fields.</h4></fieldset>";
			goto displayPage;
		}

		/* email check (performed with standard php macro) */
		$user = sanitizeString($_REQUEST['email']);
		if(! filter_var($user, FILTER_VALIDATE_EMAIL)) {
			$error = "Please, insert a valid mail.";
			goto displayPage;
		}

		/* passwords corrispondance check */
		if(strcmp($_REQUEST['password'], $_REQUEST['password_confirm']) != 0) {
			$error = "Passwords are different.";
			goto displayPage;
		}

		/* passwords validity check */
		if(preg_match("/(?=.*[a-z])(?=.*[A-Z0-9]).{0,}/m", $_REQUEST['password']) == 0) {
			$error = "Use a valid and robust password.";
			goto displayPage;
		}

		/* password is stored in hash format */
		$password = password_hash($_REQUEST['password'], PASSWORD_BCRYPT);

		$connection = connect();
		try {
			mysqli_autocommit($connection, false);
			mysqli_begin_transaction($connection);

			$statement = mysqli_stmt_init($connection);
			/* checking that the user does't not esxist yet
			** not using checkUserValidity function because there I need the FOR UPDATE
			** statement in order to prevent that the same username is used two times
			*/
			mysqli_stmt_prepare($statement, "SELECT * FROM `user` WHERE `username` = ? FOR UPDATE");
			$user = mysqli_escape_string($connection, $user);
			mysqli_stmt_bind_param($statement, 's', $user);
			if(! mysqli_stmt_execute($statement))
				throw new Exception();
			mysqli_stmt_store_result($statement);
			$numberOfRows = mysqli_stmt_num_rows($statement);
			mysqli_stmt_free_result($statement);
			mysqli_stmt_close($statement);

			if($numberOfRows == 0) {
				/* no matches, insert new user */
				$statement = mysqli_stmt_init($connection);
				mysqli_stmt_prepare($statement, "INSERT INTO `user` (`username`, `password`) VALUES (?, ?)");
				mysqli_stmt_bind_param($statement, 'ss', $user, $password);
				if(! mysqli_stmt_execute($statement))
				   throw new Exception();
				mysqli_stmt_close($statement);
				mysqli_commit($connection);
				close($connection);

				/* building new session */
				$_SESSION[$SESSION_PREFIX . 'username'] = $user;
				$_SESSION[$SESSION_PREFIX . 'authorized'] = true;
				$_SESSION[$SESSION_PREFIX . 'time'] = time();
				$response['status'] = "success";
				$response['message'] = "Correctly registered and logged in!";
				$_SESSION[$SESSION_PREFIX . 'response'] = $response;

				/* Redirect to the registered user homepage */
				header("location: index.php");
				exit();
			}
			else {
				throw new Exception();
			}
		} catch(Exception $e) {
			mysqli_rollback($connection);
			$error = "User already exists.";
		}

		mysqli_autocommit($connection, true);
		close($connection);
	}

	/* jump label */
	displayPage:
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
	<title>AirPoli</title>
	<meta name="AirPoli - SignUp" content="Website for AirPoli">
	<meta name="acco_teo" content="DP1_exam">
	<script type="text/javascript" src="functions.js"></script>
</head>
<body>
	<?php
		include('interface.php');
	?>
	<div class="main" id="main">
		<?php
			if(isset($error)) {
				echo "<fieldset class=\"error\"><h4>".sanitizeString($error)."</h4></fieldset><br>";
				unset($error);
			}
		?>
		<div id="formSignUp" class="formSignUp">
			<form action="signup.php" method="post">
				<fieldset>
					<h2>Sign Up!</h2>
					<p>Complete the form in order to create a new account.</p>
					<hr>
					<label>EMAIL</label>
					<input type="email" id="email" name="email" onchange="checkEmail();" placeholder="Please insert a valid email..">
					<label>PASSWORD</label>
					<input type="password" id="password" name="password" onkeyup="checkPassword();" placeholder="Please insert a valid password (at least one lower-case alphabetic character, and at least one alphabetical uppercase or numeric character)">
					<label>CONFIRM PASSWORD</label>
					<input type="password" id="password_confirm" name="password_confirm" onkeyup="checkPassword();" placeholder="Please confirm password..">
					<table style="width: 100%">
						<tr>
							<td><button type="submit" id="submit" name="submit" disabled="disabled">Submit</button></td>
							<td><button type="reset" onclick="resetSignUpForm();">Cancel</button></td>
						</tr>
					</table>
				</fieldset>
			</form>
		</div>
	</div>
</body>
</html>
