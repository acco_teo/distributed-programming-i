<?php
  echo '<table style="margin-left:auto; margin-right:auto; text-align: center; table-layout:fixed;">';
  /* initialize variables */
  $seatsno = $GLOBALS["FLIGHT_ROWS"] * $GLOBALS["FLIGHT_COLUMNS"];
  $freeno = $seatsno; $bookno = 0; $resvno = 0; $own_resv=0;

  /* retrieving seats staus */
  $isvalid = checkSessionValidity();
  $connection = connect();
  $seats = bookedSeats($connection);
  close($connection);

  /* the extra column and the row are added in order to
   * to display column and row indexes
   */
  for($i=0; $i<$GLOBALS["FLIGHT_ROWS"]+1; $i++) {
    $col_index = "A";
    echo "<tr>";
    for($j=0; $j<$GLOBALS["FLIGHT_COLUMNS"]+1; $j++) {
      if ($i == 0 && $j > 0) {
        /* first row is header row */
        echo "<td>".$col_index."</td>";
        $col_index = chr(ord($col_index)+1);
      }
      else if ($i > 0 && $j == 0) {
        /* first column is header column */
        echo "<td>".$i."</td>";
      }
      else if ($i != 0 && $j != 0) {
        /* middle cells */
        $id = $col_index . $i;
        if(array_key_exists($id, $seats)) {
          /* the id is already in db */
          if(strcmp($seats[$id]['status'], "R") != 0) {
            /* booked seat, no action allowed */
            $image = "src=\"./img/u-red.png\"";
            $class = "class='seat_red'";
            $action = "";
            $bookno++;
          }
          else if($isvalid && strcmp($seats[$id]['username'], $_SESSION[$SESSION_PREFIX.'username'])==0) {
            /* seat reserved and visualized by owner */
            $image = "src=\"./img/u-yellow.png\"";
            $class = "class='seat_yellow'";
            $action = "onclick=\"actionSeat('$id',this.className);\"";
            $resvno++; $own_resv++;
          }
          else {
            /* seat reserved by another user */
            $image = "src=\"./img/u-orange.png\"";
            $class = "class='seat_orange'";
            $action = "onclick=\"actionSeat('$id',this.className);\"";
            $resvno++;
          }
        }
        else {
          /* seat free */
          $image = "src=\"./img/u-green.png\"";
          $class = "class='seat_green'";
          $action = "onclick=\"actionSeat('$id',this.className);\"";
        }

        if($isvalid) {
          /* session is valid -> action on seats allowed */
          echo "<td id='".$id."'><img draggable='false' ".$class." ".$image." ".$action."></td>";
        }
        else {
          /* session not valid -> action on seats not allowed */
          echo "<td id='".$id."'><img draggable='false' ".$class." ".$image."></td>";
        }
        $col_index = chr(ord($col_index)+1);
      }
      else {
        /* (0,0 header cell), empty */
        echo "<td></td>";
      }
    }
    echo "</tr>";
  }
  echo '</table>';
  /* updating seats statistics */
  $freeno -= $resvno + $bookno;
echo <<<UPDATE_STAT
  <script type="text/javascript">
    $('#seatsno').text($("<div>"+$seatsno+"</div>").text());
    $('#freeno').text($("<div>"+$freeno+"</div>").text());
    $('#resvno').text($("<div>"+$resvno+"</div>").text());
    $('#bookno').text($("<div>"+$bookno+"</div>").text());
    updateOwnResv($own_resv);
  </script>
UPDATE_STAT;
 ?>
