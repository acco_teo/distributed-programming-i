<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="styles.css">
	<title>AirPoli</title>
	<meta name="AirPoli - index" content="Website for AirPoli">
	<meta name="author" content="acco_teo">
	<script type="text/javascript" src="functions.js"></script>
	<script src="jquery-3.3.1.min.js"></script>
</head>
<body>
	<?php
		include('interface.php');
  ?>
	<div class="main" id="main">
		<div class="stats">
			<fieldset class="statistics">
				<h3> Flight Statistics </h3>
				<table id="tab_stats">
					<tr><td>#Seats</td><td>#Free</td><td>#Booked</td><td>#Reserved</td></tr>
					<tr><td id='seatsno'></td><td id='freeno'></td><td id='bookno'></td><td id='resvno'></td></tr>
				</table>
			</fieldset>
			<br>
			<div class="info" id="info">
				<?php displayMessage(); ?>
			</div>
			<div class="rt_info" id="rt_info" style="visibility: hidden;">
				<fieldset></fieldset>
			</div>
		</div>
		<div class="seats_table" id="seats_table">
			<?php
				/* building seats table and fill statistics div */
				include('seats_map.php');
			?>
		</div>
	</div>
</body>
</html>
