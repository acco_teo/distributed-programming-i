<?php
	if(count($_COOKIE) < 1) {
		/* Cookies not enabled */
		exit();
	}
?>
		<!-- loading header -->
		<div class="header">
			<img id="icon" src="./img/plane.png" alt="PlaneIcon">
			<h1 id="title" onclick="location.reload();">AirPoli</h1>
		</div>

		<!-- JS is not enabled -->
		<noscript>
			<div id="noscript">
				<fieldset class="error" style="height:95%;">
					<h1>JS must be enabled to browse this website.<br></h1>
					<h1>Check your browser configuration.</h1>
				</fieldset>
			</div>
		</noscript>

<?php if(! checkSessionValidity()) { ?>
		<!-- menu for not authenticated users (login and signup enabled) -->
		<div class="navbar">
			<button type="button" id="home" name="home" onclick="window.location.href='./index.php'">Home</button>
			<button type="button" id="login" name="login" onclick="window.location.href='./login.php'">LogIn</button>
			<button type="button" id="signup" name="signup" onclick="window.location.href='./signup.php'">SignUp</button>
		</div>
<?php }
			else { ?>
		<!-- menu for authenticated users (welcome message and only home enabled) -->
		<div class="welcome_message">
			Welcome on board, <?php echo sanitizeString($_SESSION[$SESSION_PREFIX . 'username']); ?>
		</div>
		<div class="navbar">
			<button disabled="disabled" type="submit" id="buy" name="buy" onclick="actionBuy();">Buy</button>
			<button type="button" id="update" name="update" onclick="window.location.href='./index.php'">Update</button>
			<button type="reset" id="logout" name="logout" onclick="window.location.href='./logout.php'">Logout</button>
		</div>
<?php
			}
?>
