/* ------------------------------ JS functions ------------------------------ */

/* Local number of reserved seat, weak control of correctness berfore sending
  data to the server for buy action */
var ownResv = 0;

function actionBuy() {
  if(ownResv != 0) {
    var reservedSeats = new Array();
    $("img.seat_yellow").each( function( index ) {
      reservedSeats.push($(this).parent().attr('id'));
    });

    if(reservedSeats.length == ownResv) {
      $.ajax({
        type:"post",
        url:"./action_buy.php",
        data: {sids: JSON.stringify(reservedSeats)},
        success: function(response) {
          if(response.status == "error" && response.message == "Invalid session") {
            /* invalid session, forcing login again */
            window.location.href = "./login.php";
          }
          else {
            ownResv = 0;
            /* display response is demended to index.php both in case of error or success */
            window.location.href = "./index.php";
          }
        },
        error: function(response) {
          /* error during ajax request */
          printMessage("error", "An error occourred during server connection. Please retry.");
        }
      });
    }
    else
      printMessage("error", "An error occourred during booking. Please retry.");
  }
  else {
    printMessage("error", "Please book at least one seat.");
  }
}

function actionSeat(seatId, seatClass) {
  $('#info').css("display","none");
  $.ajax({
      type:"post",
      url:"./action_seat.php",
      data:{"sid": seatId},
      success: function(response) {
        if(response.status == "success") {
          if(isValid(response.message)) {
            /* request endend with a valid next class -> success */
            processSeatRequest(seatId, response.message, seatClass);
          }
          else {
            /* something gone wrong (success without class indication) */
            printMessage("error", "An error occourred. Please retry.");
          }
        }
        else {
          /* request endend with a not valid next class -> unsuccess */
          if(response.status == "error" && response.message == "Invalid session")
            window.location.href = "./login.php";
          printMessage("error", response.message);
        }
      },
      error: function(response) {
        /* error during ajax request */
        printMessage("error", "An error occourred during server connection. Please retry.");
      }
  });
}

function checkEmail() {
  var user_email = document.getElementById("email");

  /* credits to https://emailregex.com */
  const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  /* adjusting input fields border in accord to email validity */
  if(regexMail.test(user_email.value.toLowerCase())) {
    user_email.style.border = "1px solid green";
  }
  else {
    user_email.style.border = "1px solid red";
  }
}

function checkPassword() {
  var password = document.getElementById("password");
  var confirm_password = document.getElementById("password_confirm");

  /* Regexp lowercase letters */
  var lcLetters = /[a-z]/g;
  /* Regexp capital letters and/or numbers */
  var ucLetters = /[A-Z0-9]/g;

  /* adjusting input fields border in accord to password validity */
  if(password.value.match(lcLetters) && password.value.match(ucLetters)) {
    password.style.border = "1px solid green";
    if(password.value == confirm_password.value) {
      confirm_password.style.border = "1px solid green";
      document.getElementById("submit").disabled = false;
    }
    else {
      confirm_password.style.border = "1px solid red";
      document.getElementById("submit").disabled = true;
    }
  }
  else {
    password.style.border = "1px solid red";
    confirm_password.style.border = "1px solid red";
    document.getElementById("submit").disabled = true;
  }
}

function isValid(responseClass) {
  if(responseClass.indexOf("seat_") != -1)
    return true;
  else
    return false;
}

function printMessage(messageClass, string) {
  $('#rt_info').css("visibility", "visible");
  $('#rt_info > fieldset').attr("class", messageClass);
  /* trying yo avoid code injection $(string).text() */
  $('#rt_info > fieldset').text($("<div>"+string+"</div>").text());
}

function processSeatRequest(seatId, newSeatClass, oldSeatClass) {
  /* oldSeatClass is passed just for real time statistics */
  var freeno = parseInt($('#freeno').text());
  var resvno = parseInt($('#resvno').text());
  var bookno = parseInt($('#bookno').text());
  switch (newSeatClass) {
    case "seat_green":
      $('#'+seatId+' > img').attr({
        "class" : "seat_green",
        "src" : "./img/u-green.png",
        "onclick" : "actionSeat('"+seatId+"',this.className);"
      });
      printMessage("success", "Seat "+seatId+" correctly freed!");
      freeno++; resvno--; ownResv--;
    break;
    case "seat_yellow":
      $('#'+seatId+' > img').attr({
        "class" : "seat_yellow",
        "src" : "./img/u-yellow.png",
        "onclick" : "actionSeat('"+seatId+"',this.className);"
      });
      printMessage("success", "Seat "+seatId+" correctly reserved!");
      ownResv++;
      if(oldSeatClass == "seat_green"){freeno--; resvno++;}
    break;
    case "seat_red":
      $('#'+seatId+' > img').attr({
        "class" : "seat_red",
        "src" : "./img/u-red.png"
      });
      $('#'+seatId+' > img').removeAttr("onclick");
      printMessage("error", "Sorry, seat "+seatId+" has been already booked.");
      bookno++;
      oldSeatClass == "seat_green" ? freeno-- : resvno--;
    break;
  }
  setBuyButton();
  $('#freeno').text($("<div>"+freeno+"</div>").text());
  $('#resvno').text($("<div>"+resvno+"</div>").text());
  $('#bookno').text($("<div>"+bookno+"</div>").text());
}

/* custom reset function, I need to reset colored borders */
function resetSignUpForm() {
  var user_email = document.getElementById("email");
  var password = document.getElementById("password");
  var confirm_password = document.getElementById("password_confirm");
  document.getElementById("submit").disabled = true;
  user_email.style.border = "";
  password.style.border = "";
  confirm_password.style.border = "";
}

/* enabling and disabling buy button in accord to reserved seat number */
function setBuyButton() {
  if(ownResv > 0)
    $('#buy').removeAttr("disabled");
  else
    $('#buy').attr("disabled", "disabled");
}

/* update reserved seat number after map creation */
function updateOwnResv(num) {
  ownResv += parseInt(num);
  setBuyButton();
}
