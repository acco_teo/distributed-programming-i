<?php
/* ------------------------ CONFIGURATION ------------------------- */
$host = "";                 /* host address                         */
$userDB = "";               /* database user name                   */
$passwordDB = "";           /* database user password               */
$database = "";             /* database prefix name                 */
$FLIGHT_ROWS = 10;          /* plane rows number (MAX 99)           */
$FLIGHT_COLUMNS = 6;        /* plane columns number (MAX 26)        */
$TIMEOUT = 120;             /* session/cookies validity             */
$SESSION_PREFIX = "";       /* session prefix name                  */
/* ---------------------------------------------------------------- */

function bookedSeats($connection) {
	$seats = array();
	$result = mysqli_query($connection, "SELECT * FROM `seat` ORDER BY `id` FOR UPDATE");
	$rows = mysqli_num_rows($result);
	for($i = 0; $i < $rows; $i++) {
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		$id = $row['id'];
		$seats[$id]['username'] = $row['username'];
		$seats[$id]['status'] = $row['status'];
	}
	mysqli_free_result($result);
	return $seats;
}

/* function checkCookies()
 * check if cookies are enabled
 */
function checkCookies() {
	if(isset($_GET['cookiecheck'])) {
		if(isset($_COOKIE['testcookie'])) {
			/* Cookies enabled, load the page without the "bad" part */
			header("Location: " . str_replace("?cookiecheck=1", "", $_SERVER['PHP_SELF']));
		} else {
			/* Cookies disabled */
			header("Location: nocookies.php");
			exit();
		}
	} else {
		if(! isset($_COOKIE['testcookie'])) {
			/* Here only when loading page the first time, try to set a cookie */
			setcookie('testcookie', "testvalue");
			header("Location: " . $_SERVER['PHP_SELF'] . "?cookiecheck=1");
			exit();
		}
	}
}

function checkSeatValidity($seat_id) {
	$column = substr($seat_id, 0, 1);
	if(strnatcmp($column, "A") >= 0 && strnatcmp($column, chr(ord('A')+$GLOBALS["FLIGHT_COLUMNS"]-1)) <= 0) {
		if(strnatcmp($seat_id, "A1") >=0 && strnatcmp($seat_id, $column.$GLOBALS["FLIGHT_ROWS"]) <= 0)
			return true;
		else
			return false;
	}
	else {
		return false;
	}
}

function checkSessionValidity() {
	$SESSION_PREFIX = $GLOBALS['SESSION_PREFIX'];
	if(isset($_SESSION[$SESSION_PREFIX . 'authorized']) && isset($_SESSION[$SESSION_PREFIX . 'username'])) {
		if(time() - $_SESSION[$SESSION_PREFIX . 'time'] > $GLOBALS['TIMEOUT']) { // Timeout expired
			$_SESSION = array();
			// If it's desired to kill the session, also delete the session cookie.
			// Note: This will destroy the session, and not just the session data!
			if (ini_get("session.use_cookies")) {
				$params = session_get_cookie_params();
				setcookie(session_name(), '', time() - 3600*24,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
				);
			}
			session_destroy(); // Destroy session
			return false;
		} else {
			$_SESSION[$SESSION_PREFIX . 'time'] = time();
			return true;
		}
	}
	return false;
}

function checkUserValidity($connection, $usr) {
	$statement = mysqli_stmt_init($connection);
	mysqli_stmt_prepare($statement, "SELECT * FROM `user` WHERE `username` = ?");
	$usr = mysqli_escape_string($connection, $usr);
	mysqli_stmt_bind_param($statement, 's', $usr);
	mysqli_stmt_execute($statement);
	mysqli_stmt_store_result($statement);
	$numberOfRows = mysqli_stmt_num_rows($statement);
	mysqli_stmt_free_result($statement);
	mysqli_stmt_close($statement);
	if($numberOfRows == 1)
		return true;
	else
		return false;
}

/* function connect()
 * tries to connect to the db specified by CONFIGURATION part
 */
function connect() {
	$connection = mysqli_connect($GLOBALS["host"], $GLOBALS["userDB"], $GLOBALS["passwordDB"], $GLOBALS["database"])
		or die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

	return $connection;
}

/* function close()
 * tries to close the connection passed as parameter
 * @param $connection: connection to close
 */
function close($connection) {
	mysqli_close($connection)
		or die("Close error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());
}

function displayMessage() {
	$SESSION_PREFIX = $GLOBALS['SESSION_PREFIX'];
	if(isset($_SESSION[$SESSION_PREFIX.'response'])) {
		/* there is a message to display */
		$tmp = $_SESSION[$SESSION_PREFIX.'response'];
		if(strcmp($tmp['status'], "success") == 0) {
			/* success message */
			echo "<fieldset class=\"success\"><h4>".sanitizeString($tmp['message'])."</h4></fieldset>";
		}
		else if(strcmp($_SESSION[$SESSION_PREFIX.'response']['status'], "error") == 0) {
			/* error message */
			echo "<fieldset class=\"error\"><h4>".sanitizeString($tmp['message'])."</h4></fieldset>";
		}
		/* if something doen't respect the protocol, unset it */
		unset($_SESSION[$SESSION_PREFIX.'response']);
	}
}

/* function redirectHTTPS()
 * force the usage of HTTPS slightly evreywhere
 */
function redirectHTTPS() {
	if($_SERVER['HTTPS'] != "on") {
		$redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		header("location: $redirect");
	}
}

function sanitizeString($var) {
	$var = strip_tags($var);
	$var = htmlentities($var);
	$var = stripslashes($var);
	return $var;
}

?>
