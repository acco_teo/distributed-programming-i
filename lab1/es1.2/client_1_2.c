/*
 *	File dumb_client.c
 *      TCP CLIENT with the following features:
 *      - Gets server IP address (IPv4) and port from keyboard
 *      - Informs the user if it has managed to perform the connection or not
 *      - Closes the connection and terminates
 */


#include     <stdlib.h>
#include     <string.h>
#include     <inttypes.h>
#include     "errlib.h"
#include     "sockwrap.h"

#define BUFLEN	128 /* BUFFER LENGTH */

/* GLOBAL VARIABLES */
char *prog_name;

int main(int argc, char *argv[])
{
    uint16_t	   tport_n, tport_h;	/* server port number (net/host ord) */

    int		   s;
    int		   result;
    struct sockaddr_in	saddr;	/* server address structure */
    struct in_addr	sIPaddr; 	/* server IP addr. structure */

    if(argc < 3)
      err_quit("Usage: ./dumb_client ip_address port_number");

    prog_name = argv[0];
    
    /* input IP address and port of server */
    result = inet_aton(argv[1], &sIPaddr);  /* convert IP address */
    if (!result)
        err_quit("Invalid address");

    if (sscanf(argv[2], "%" SCNu16, &tport_h) != 1) /* % SCNu16 because we don't know integer length SCNu16 is a macro */
        err_quit("Invalid port number");
    tport_n = htons(tport_h);

    /* create the socket */
    printf("Creating socket\n");
    s = Socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    printf("done. Socket fd number: %d\n", s);

    /* prepare address structure */
    bzero(&saddr, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port   = tport_n;
    saddr.sin_addr   = sIPaddr;

    /* connect */
    showAddr("Connecting to target address", &saddr);
    Connect(s, (struct sockaddr *) &saddr, sizeof(saddr));
    printf("done.\n");

    printf("The connection will be closed..\n");

    Close(s);

    exit(0);
}
