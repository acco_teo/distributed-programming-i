/*
 * 	File Server0u.c
 *	ECHO UDP SERVER with the following features:
 *      - Gets port from keyboard
 *      - SEQUENTIAL: serves one request at a time
 */


#include    <stdlib.h>
#include    <string.h>
#include    <inttypes.h>
#include    "errlib.h"
#include    "sockwrap.h"

#define BUFLEN		65536 /* Maximum UDP datagram length */

/* GLOBAL VARIABLES */
char *prog_name;

int main(int argc, char *argv[])
{
    char	 	buf[BUFLEN];		/* reception buffer */
    uint16_t 		lport_n, lport_h;	/* port where server listens */
    int			s;
    int			n;
    socklen_t		addrlen;
    struct sockaddr_in	saddr, from;

    prog_name = argv[0];

    if (argc != 2)
    {
      printf("Usage: %s <port number>\n", prog_name);
      exit(1);
    }

    /* get server port number */
    if (sscanf(argv[1], "%" SCNu16, &lport_h) != 1)
    	err_sys("Invalid port number");
    lport_n = htons(lport_h);

    /* create the socket */
    printf("creating socket\n");
    s = Socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    printf("done, socket number %u\n",s);

    /* bind the socket to all local IP addresses */
    bzero(&saddr, sizeof(saddr));
    saddr.sin_family      = AF_INET;
    saddr.sin_port        = lport_n;
    saddr.sin_addr.s_addr = INADDR_ANY;
    showAddr("Binding to address", &saddr);
    Bind(s, (struct sockaddr *) &saddr, sizeof(saddr));
    printf("done.\n");

    /* main server loop */
    for (;;)
    {
        addrlen = sizeof(struct sockaddr_in);
        n = recvfrom(s, buf, BUFLEN-1, 0, (struct sockaddr *)&from, &addrlen);
        if (n != -1)
        {
          buf[n] = '\0';
          showAddr("Received message from", &from);
          printf(": [%s]\n", buf);

          if(sendto(s, buf, n, 0, (struct sockaddr *)&from, addrlen) != n)
            printf("Write error while replying\n");
          else
            printf("Reply sent\n");
        }
    }
}
