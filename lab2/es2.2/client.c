/*
 *	File Client1u.C
 *      ECHO UDP CLIENT with the following feqatures:
 *      - Gets server IP address and port from keyboard
 *      - LINE/ORIENTED:
 *        > continuously reads lines from keyboard
 *        > sends each line to the server
 *        > waits for response (at most for a fixed amount of time) and diaplays it
 *      - Terminates when the "close" or "stop" line is entered
 */


#include     <stdlib.h>
#include     <string.h>
#include     <inttypes.h>
#include     "errlib.h"
#include     "sockwrap.h"

#define BUFLEN 128  /* BUFFER LENGTH */
#define TIMEOUT  5  /* TIMEOUT (seconds) */

/* GLOBAL VARIABLES */
char *prog_name;


int main(int argc, char *argv[])
{
    char	 	rbuf[BUFLEN];	   /* reception buffer */

    uint32_t		taddr_n;  /* server IP addr. (net/host ord) */
    uint16_t		tport_n, tport_h;  /* server port number */

    int		s;
    struct sockaddr_in	saddr;
    fd_set		cset;
    struct timeval	tval;

    prog_name = argv[0];

    /* ./client14 ip_address port_number */

    if(argc != 4)
      err_sys("Usage: %s ip_address port_number", prog_name);

    /* retrieving the ip address */
    taddr_n = inet_addr(argv[1]);
    if (taddr_n == INADDR_NONE)
      err_sys("Invalid ip address");

    /* retrieving port number */
    if (sscanf(argv[2], "%" SCNu16, &tport_h)!=1)
      err_sys("Invalid port number");
    tport_n = htons(tport_h);

    /* create the UDP socket */
    printf("Creating socket\n");
    s = Socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    printf("done. Socket number: %d\n",s);

    /* prepare server address structure */
    bzero(&saddr, sizeof(saddr));
    saddr.sin_family      = AF_INET;
    saddr.sin_port        = tport_n;
    saddr.sin_addr.s_addr = taddr_n;

    /* starting trasmission */
    uint32_t number;
    int e_n;
    size_t len, n;
    struct sockaddr_in from;
    socklen_t fromlen;

    printf("Insert 32 bit unsigned integer: ");
    if(scanf("%lo", &number) != 1)
      err_sys("Expected an unsigned 32 bit integer");

    n = sendto(s, argv[3], len, 0, (struct sockaddr *) &saddr, sizeof(saddr));
    if (n != len)
      err_sys("Write error");

    printf("Waiting for response...\n");
    FD_ZERO(&cset);
    FD_SET(s, &cset);
    tval.tv_sec = TIMEOUT;
    tval.tv_usec = 0;

    n = Select(FD_SETSIZE, &cset, NULL, NULL, &tval);

    if (n > 0) {
      /* receive datagram */
      fromlen = sizeof(struct sockaddr_in);
      n = recvfrom(s, rbuf, BUFLEN, 0, (struct sockaddr *)&from, &fromlen);
      if (n != -1) {
        rbuf[n] = '\0';
        showAddr("Received response from", &from);
        printf(": [%s]\n", rbuf);
        break ;
      }
      else
        err_sys("Error in receiving response");
    }
    else
        printf("No response received after %d seconds\n", TIMEOUT);
    close(s);
    exit(0);
}
