function greatest(/**/) {
  var max; var i=0;

  if(isNaN(arguments[i]))
  	max = 0;
  else
  	max = arguments[i];

  for(i=1; i<arguments.length; i++) {
  	if(isNaN(arguments[i]))
  		continue;
  	if(max < arguments[i])
  		max = arguments[i];
  }
  return max;
}
